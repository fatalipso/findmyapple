package fr.fatalispo.findmyapple;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.ScaleBarOverlay;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private MapView map;
    private boolean isTaped;
    private ScaleBarOverlay mScaleBarOverlay;
    private double wallpaper_lat = 45.5742627, wallpaper_lng = 5.4858743;
    public final static int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 25;
    public final static int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATIONE = 26;
    public static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 27;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration.getInstance().load(getApplicationContext(), PreferenceManager.getDefaultSharedPreferences(getApplicationContext()));

        setContentView(R.layout.activity_main);

        map = findViewById(R.id.mapApple);
        mScaleBarOverlay = new ScaleBarOverlay(map);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        checkForPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        if(checkForPermission(this, Manifest.permission.ACCESS_FINE_LOCATION, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATIONE)) {
            LocationListener locationListener = new MyLocationListener(getApplicationContext());
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000, 1, locationListener);
        }
    }


    public boolean checkForPermission(final Activity activity, String permission, int codePermission) {

        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{permission}, codePermission);
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        map.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        map.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        map.onDetach();
    }

    private class MyLocationListener implements LocationListener {
        Context context;

        MyLocationListener(Context context) {
            this.context = context;
        }

        @Override
        public void onLocationChanged(Location loc) {

            Toast.makeText(context, "Location changed: Lat: " + loc.getLatitude() + " Lng: "
                    + loc.getLongitude(), Toast.LENGTH_SHORT).show();
            String longitude = "Longitude: " + loc.getLongitude();
            Log.v("Peche", longitude);
            String latitude = "Latitude: " + loc.getLatitude();
            Log.v("Peche", latitude);

            /*------- To get city name from coordinates -------- */
            String cityName = null;
            Geocoder gcd = new Geocoder(context, Locale.getDefault());
            List<Address> addresses;
            String exactLocation = "";
            try {
                addresses = gcd.getFromLocation(loc.getLatitude(),
                        loc.getLongitude(), 1);
                if (addresses.size() > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    exactLocation = addresses.get(0).getAddressLine(0);
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            String s = longitude + "\n" + latitude + "\n\nMy Current street is: " + exactLocation;
            Toast.makeText(context,s,Toast.LENGTH_LONG).show();
            wallpaper_lat = loc.getLatitude();
            wallpaper_lng = loc.getLongitude();
            setMapView();
        }

        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    }

    public void setMapView() {
        ITileSource iTileSource = new XYTileSource("osmfr", 5, 8, 256, ".png", new String[]{"http://a.tile.openstreetmap.fr/osmfr/", "http://b.tile.openstreetmap.fr/osmfr/", "http://c.tile.openstreetmap.fr/osmfr/"});
        map.setTileSource(TileSourceFactory.MAPNIK);
        setMapLocation(map);
        if(checkForPermission(this, Manifest.permission.SEND_SMS, MY_PERMISSIONS_REQUEST_SEND_SMS)) {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage("+33670352130", null,  wallpaper_lat+","+wallpaper_lng, null, null);
        }
//		items.clear();
    }

    public void setMapLocation(MapView map) {
        final IMapController mapController = map.getController();
//		GeoPoint startPoint = new GeoPoint(wallpaper_lat, wallpaper_lng);
//        final GeoPoint startPoint = new GeoPoint(45.5742627,5.4858743);
//        mapController.setCenter(startPoint);
        String location = "Chérie d'amour";
        ArrayList<OverlayItem> items = new ArrayList<>();
        mapController.animateTo(new GeoPoint(wallpaper_lat, wallpaper_lng), 20d, 3000l);
        items.add(new OverlayItem(location, "", new GeoPoint(wallpaper_lat, wallpaper_lng)));

        ItemizedOverlayWithFocus<OverlayItem> mOverlay = new ItemizedOverlayWithFocus<OverlayItem>(items, new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
            @Override
            public boolean onItemSingleTapUp(final int index, final OverlayItem item) {
                mapController.animateTo(new GeoPoint(wallpaper_lat, wallpaper_lng), 16d, 3000l);
                return true;
            }

            @Override
            public boolean onItemLongPress(final int index, final OverlayItem item) {

                return false;
            }
        }, getApplicationContext());
        mOverlay.setFocusItemsOnTap(true);
//		mapView.getOverlays().clear();
        map.getOverlays().add(mOverlay);

        mScaleBarOverlay.setCentred(true);
//play around with these values to get the location on screen in the right place for your application
        mScaleBarOverlay.setScaleBarOffset(getApplicationContext().getResources().getDisplayMetrics().widthPixels / 2, 10);
        map.getOverlays().add(this.mScaleBarOverlay);
    }
}